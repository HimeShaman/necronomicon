import {Contact} from "../model/contact.model";
import {City} from "../model/city.model";
import {Divinity} from "../model/divinity.model";
import {Avatar} from "../model/avatar.model";

export const getContacts = async (query: string): Promise<Contact[]> => {
    return fetch(`http://localhost:8080/contacts?q=${query}`)
        .then(response => response.json())
        .catch((error) => console.error(error))
}

export const updateContact = async (contact: Contact): Promise<any> => {
    return fetch(`http://localhost:8080/contacts/${contact.id}`, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(contact)
    })
        .then(response => response.json())
        .catch((error) => console.error(error))
}

export const createContact = async (contact: Contact): Promise<any> => {
    return fetch(`http://localhost:8080/contacts`, {
        method: "POST",
        body: JSON.stringify(contact),
        headers: {
            "Content-Type": "application/json",
        },
    })
        .then(response => response.json())
        .catch((error) => console.error(error))
}

export const annihilateContact = async (contactId: number): Promise<any> => {
    return fetch(`http://localhost:8080/contacts/${contactId}`, {
        method: "DELETE",
    })
        .then(response => response.json())
        .catch((error) => console.error(error))
}


export const getCities = async (): Promise<City[]> => {
    return fetch(`http://localhost:8080/cities`)
        .then(response => response.json())
        .catch((error) => console.error(error))
}

export const getDivinities = async (): Promise<Divinity[]> => {
    return fetch(`http://localhost:8080/divinities`)
        .then(response => response.json())
        .catch((error) => console.log(error))
}

export const getAvatars = async (): Promise<Avatar[]> => {
    return fetch(`http://localhost:8080/avatars`)
        .then((response) => response.json())
        .catch((error) => console.log(error))

}

