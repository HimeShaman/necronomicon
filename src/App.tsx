import React from "react";
import "./App.css";
import {ContactList} from "./component/contactList/contactList.component";
import {SearchBar} from "./component/searchBar/searchBar.component";
import {ContactDrawer} from "./component/contactDrawer/contactDrawer.component";
import {DrawerProvider} from "./context/drawer.context";
import {ContactProvider} from "./context/contact.context";

function App() {

    return (
        <div className="App">
            <header>
                <h1 className="flex justify-center text-7xl text-white">Necronomicon'tact</h1>
            </header>
            <ContactProvider>
                <SearchBar/>
                <DrawerProvider>
                    <ContactDrawer/>
                    <ContactList/>
                </DrawerProvider>
            </ContactProvider>
        </div>
    );
}

export default App;
