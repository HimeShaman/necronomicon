import * as React from "react";
import {useContext} from "react";
import {DrawerContext} from "../../context/drawer.context";
import {ContactContext} from "../../context/contact.context";
import {Avatar} from "../../model/avatar.model";
import moment from "moment";


type ContactCardProps = {
    id: number,
    firstname: string,
    lastname: string,
    email: string,
    birthdate: Date,
    affliction: string,
    avatar: Avatar
};

const ContactCard = ({id, firstname, lastname, email, birthdate, affliction, avatar}: ContactCardProps) => {
    const drawerContext = useContext(DrawerContext)
    return (
        <div className="container flex flex-col md:flex-row
        bg-white bg-opacity-10 backdrop-filter backdrop-blur-md drop-shadow
        hover:bg-opacity-20
        border border-white border-opacity-20
        rounded-lg shadow-md md:h-40 w-3/5 p-5
        contactCard
        " onClick={() => {
            drawerContext?.setOpened(true, {type: "edit", contactId: id})
        }}>
            <a target="_blank" rel="noreferrer" className="flex md:w-1/4 justify-center">
                <img
                    src={avatar.url}
                    alt="Avatar"
                    className="flex rounded-full md:w-32 md:h-32 w-24 h-24 align-middle shadow-md"
                />
            </a>
            <div className="flex flex-col w-full md:w-3/4 space-y-1 align-middle text-white px-2">
                <h2 className="flex font-semibold text-green-600 justify-center md:justify-start">
                    {firstname} {lastname}
                </h2>
                <p className="flex justify-center md:justify-start"> {email}</p>
                <p className="flex justify-center md:justify-start">Né.e le {moment(birthdate).format("DD/MM")}</p>
                <p className="flex justify-center md:justify-start">
                    Touché.e par {affliction}
                </p>
            </div>
        </div>
    );
};

export const ContactList = () => {
    const drawerContext = useContext(DrawerContext)
    const contactContext = useContext(ContactContext)
    const list = contactContext?.contacts
    return (
        <div className="container flex flex-col md:w-5/6 w-full space-y-3 content-center mx-auto lg items-center">
            <button type="button"
                    className="rounded-full
                    border border-white border-opacity-20 bg-opacity-50 shadow-md
                    text-white font-semibold w-48 h-10 flex item-center align-middle items-center"
                    id="newButton"
                    onClick={() => drawerContext?.setOpened(true, {type: "create"})}>
                <div className="flex items-center px-3">
                    <img id="newIcon" className="flex m-1" src="https://i.ibb.co/dcRWj69/buttonplus.png"
                         alt="buttonplus"/>
                    <span className="flex">Nouveau contact</span>
                </div>
            </button>
            {list?.map((contactItem) => (
                <ContactCard
                    id={contactItem.id}
                    key={contactItem.id}
                    firstname={contactItem.firstname}
                    lastname={contactItem.lastname}
                    email={contactItem.email}
                    birthdate={new Date(contactItem.birthdate)}
                    affliction={contactItem.affliction}
                    avatar={contactItem.avatar}
                />
            ))}
        </div>
    )
}
