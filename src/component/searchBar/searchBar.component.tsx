import * as React from "react";
import {useContext} from "react";
import {ContactContext} from "../../context/contact.context";

export const SearchBar = () => {
    const contactContext = useContext(ContactContext)
    return (
        <div className="flex justify-center my-2">
            <input
                placeholder="Rechercher une entité obscure..."
                className="flex bg-black bg-opacity-50 rounded-full px-4 h-8 md:w-1/2 w-3/4 text-white placeholder-grey-200
                focus:outline-none focus:ring focus:ring-white focus:ring-opacity-10 shadow-inner"
                type="text"
                name="searchInput"
                onChange={(e) => contactContext?.findContacts(e.target.value)}
            />
        </div>
    );
};
