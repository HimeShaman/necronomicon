import {DrawerContext, EditContactAction} from "../../context/drawer.context";
import "./contactDrawer.css";
import React, {useContext, useEffect, useState} from "react";
import {ContactContext} from "../../context/contact.context";
import {FormContact} from "../../model/contact.model";
import Select from "react-select";
import makeAnimated from "react-select/animated";
import moment from "moment";
import {annihilateContact} from "../../service/client.service";
import {multiSelectStyles} from "./multi-select-style";

type ContactState = {
    contact?: FormContact,
}
export const ContactDrawer = () => {

    const drawerContext = useContext(DrawerContext)

    return (
        <div id="drawer" className={drawerContext && drawerContext.opened ? `h-screen drop-shadow-md  bg-white bg-opacity-20 
            backdrop-filter backdrop-blur-lg drop-shadow
            border border-white border-opacity-20 rounded-lg
            h-screen w-11/12 md:w-3/5 z-50 fixed m-0 flex flex-col p-10 anim shown-drawer` :
            "hidden-drawer anim fixed"}>
            {drawerContext?.opened && <ContactForm/>}
        </div>
    )
}

const ContactForm = () => {

    const [state, setState] = useState<ContactState>({
        contact: {},
    })

    const drawerContext = useContext(DrawerContext)
    const contactContext = useContext(ContactContext)

    useEffect(() => {
        //TODO: factoriser les finds
        if (drawerContext?.action?.type === "edit") {
            const action = drawerContext.action as EditContactAction
            const contact = contactContext?.contacts.find((contact) => contact.id === action.contactId)
            setState({...state, contact})
        }
        if (drawerContext?.action?.type === "create") {
            setState({...state, contact: {...state.contact, divinities: []}})
        }
    }, [])

    function onAvatarChange(event: any) {
        const newAvatar = drawerContext?.avatars.find((avatar) =>
            avatar.id === Number(event.target.value)
        )
        setState({...state, contact: {...state.contact, avatar: newAvatar}})
    }

    function onBirthplaceChange(event: any) {
        const newBirthPlace = drawerContext?.cities.find((city) =>
            city.id === Number(event.target.value)
        )
        setState({...state, contact: {...state.contact, birthplace: newBirthPlace}})
    }

    function onDivinitiesChange(event: any) {
        const newDivinities = event.map((divinity: any) => {
            return {id: divinity.value, name: divinity.label}
        })
        setState({...state, contact: {...state.contact, divinities: newDivinities}})
    }

    function saveContact() {
        drawerContext?.saveContact(state!.contact!)
    }

    function deleteContact() {
        if (drawerContext?.action?.type === "edit") {
            const id = drawerContext.action.contactId
            annihilateContact(id)
                .then(() => {
                    contactContext?.removeContact(id)
                    drawerContext.setOpened(false, null)
                })
        }
    }

    return (
        <div className="space-y-3">
            <button type="button" className="top-2 right-2 absolute closeDrawer rounded-full bg-red-600 w-5"
                    onClick={() => drawerContext?.setOpened(false, null)}>
                <img id="closeDrawer" src="https://i.ibb.co/mGQ9cZ1/cancel.png" alt="cancel"/>
            </button>
            <label className="flex flex-col">
                <p className="flex align-middle font-semibold mr-4 text-white">Avatar</p>
                <div className="flex flex-row">
                    {drawerContext?.avatars.map((avatar) => {
                        return <div key={avatar.id}>
                            <label>
                            <input type="radio" name="avatarSelect" value={avatar.id}
                                   checked={avatar.id === state.contact?.avatar?.id}
                                   onChange={onAvatarChange} key={avatar.id} className="hidden-avatar-check"
                            />
                            <img src={avatar.url} alt={avatar.alt} className="h-24 w-24 mx-2 rounded-full
                            border border-white border-opacity-20 opacity-90 shadow-inner"/>
                            </label>
                        </div>
                    })}
                </div>
            </label>
            <label className="flex flex-col">
                <p className="flex align-baseline font-semibold mr-4 text-white">Prénom</p>
                <input type="text"
                       className="flex w-1/2 px-2
                       drawer-input rounded-full focus:outline-none focus:ring focus:ring-white focus:ring-opacity-10
                       border border-white border-opacity-20 bg-opacity-50"
                       value={state?.contact?.firstname || ''}
                       onChange={(event) => setState({
                           ...state,
                           contact: {...state.contact, firstname: event.target.value}
                       })}/>
            </label>
            <label className="flex flex-col">
                <p className="flex align-baseline font-semibold mr-4 text-white">Nom</p>
                <input type="text"
                       className="flex w-1/2 px-2
                       drawer-input rounded-full focus:outline-none focus:ring focus:ring-white focus:ring-opacity-10
                       border border-white border-opacity-20 bg-opacity-50"
                       value={state?.contact?.lastname || ''}
                       onChange={(event) => setState({
                           ...state,
                           contact: {...state.contact, lastname: event.target.value}
                       })}/>
            </label>
            <label className="flex flex-col">
                <p className="flex align-baseline font-semibold mr-4 text-white">Email</p>
                <input type="mail"
                       className="flex w-1/2 px-2
                       drawer-input rounded-full focus:outline-none focus:ring focus:ring-white focus:ring-opacity-10
                       border border-white border-opacity-20 bg-opacity-50"
                       value={state?.contact?.email || ''}
                       onChange={(event) => setState({
                           ...state,
                           contact: {...state.contact, email: event.target.value}
                       })}/>
            </label>
            <label className="flex flex-col">
                <p className="flex align-middle font-semibold mr-4 text-white">Date de naissance</p>
                <input type="date"
                       className="flex w-1/2 px-2
                       drawer-input rounded-full focus:outline-none focus:ring focus:ring-white focus:ring-opacity-10
                       border border-white border-opacity-20 bg-opacity-50"
                       value={moment(state?.contact?.birthdate).format("YYYY-MM-DD" || moment().format("YYYY-MM-DD"))}
                       onChange={(event) => setState({
                           ...state,
                           contact: {...state.contact, birthdate: new Date(event.target.value)}
                       })}/>
            </label>
            <label className="flex flex-col">
                <p className="flex align-middle font-semibold mr-4 text-white">Lieu de naissance</p>
                <select value={state?.contact?.birthplace?.id || ''} className="flex w-1/2 px-2
                       drawer-input rounded-full focus:outline-none focus:ring focus:ring-white focus:ring-opacity-10
                       border border-white border-opacity-20 bg-opacity-50 select"
                        onChange={onBirthplaceChange}>
                    <option value="">Aucun sélectionné</option>
                    {drawerContext?.cities?.map((cityItem) =>
                        <option value={cityItem.id}
                                key={cityItem.id}>{cityItem.name}</option>
                    )
                    }
                </select>
            </label>
            <label className="flex flex-col">
                <p className="flex align-middle font-semibold mr-4 text-white">Divinités vénérées</p>
                {state.contact?.divinities &&
                <Select isMulti components={makeAnimated()} className="w-1/2 px-2
                       rounded-full focus:outline-none focus:ring focus:ring-white focus:ring-opacity-10"
                        onChange={onDivinitiesChange} styles={multiSelectStyles}
                        defaultValue={state.contact?.divinities?.map((divinity) => {
                            return {value: divinity.id, label: divinity.name}
                        }) || {value: "", label: ""}}
                        options={drawerContext?.divinities.map((divinity) => {
                            return {value: divinity.id, label: divinity.name}
                        })}/>}
            </label>
            <label className="flex flex-col">
                <p className="flex align-middle font-semibold mr-4 text-white">Affliction</p>
                <input type="text"
                       className="flex w-1/2 px-2
                       drawer-input rounded-full focus:outline-none focus:ring focus:ring-white focus:ring-opacity-10
                       border border-white border-opacity-20 bg-opacity-50"
                       value={state?.contact?.affliction || ""}
                       onChange={(event) => setState({
                           ...state,
                           contact: {...state.contact, affliction: event.target.value}
                       })}/>
            </label>
            <label className="flex flex-col">
                <p className="flex align-middle font-semibold mr-4 text-white">Etendue de l'affliction</p>
                <input type="range" min="0" max="10" value={state?.contact?.afflictionLevel || 0}
                       onChange={(event) => setState({
                           ...state,
                           contact: {...state.contact, afflictionLevel: Number(event.target.value)}
                       })}/>
            </label>
            <button type="button"
                    className="rounded bg-gradient-to-r from-green-900 via-green-600 to-blue-400 text-white font-semibold w-48 h-8 flex item-center justify-center m-auto"
                    onClick={saveContact}><span className="flex align-middle">Enregistrer</span>
            </button>
            {drawerContext?.action?.type === "edit" &&
            <button type="button"
                    className="rounded bg-gradient-to-r from-red-900 via-red-600 to-red-400 text-white font-semibold w-48 h-8 flex item-center justify-center m-auto"
                    onClick={deleteContact}><span className="flex align-middle">Supprimer</span></button>}
        </div>
    )
}