export const multiSelectStyles = {
    control: (styles: any, state: any) => ({
        ...styles,
        backgroundColor: 'rgba(255, 255, 255, 0.5)',
        borderRadius: '9999px',
        border: 'solid 1px rgba(255, 255, 255, 0.2)',
        boxShadow: state.isFocused ? null : null,
        paddingLeft: '10px'
    }),
    dropdownIndicator: (styles: any) => ({
        ...styles,
        color: 'rgb(62, 62, 62)'
    }),
    multiValueRemove: (styles: any) => ({
        ...styles,
        color: 'rgb(62, 62, 62)'
    }),
    option: (styles: { [x: string]: any; }, {data, isDisabled, isFocused, isSelected, isActive}: any) => {
        return {
            ...styles,
            backgroundColor: isDisabled
                ? ''
                : isSelected
                    ? 'rgba(4, 196, 217, 0.3)'
                    : isFocused
                        ? 'rgba(4, 196, 217, 0.3)'
                        : isActive
                            ? 'rgba(4, 196, 217, 0.3)' : ''
            ,
        };
    },
    multiValue: (styles: any, {data}: any) => {
        return {
            ...styles,
            backgroundColor: 'rgba(250, 250, 250, 0.9)'
        };
    },
    menu: (styles: any, {data}: any) => {
        return {
            ...styles,
            backgroundColor: 'rgba(250, 250, 250, 0.9)',
            boxShadow: '0 1px 3px 0 rgba(0, 0, 0, 0.5)'
        };
    },
};