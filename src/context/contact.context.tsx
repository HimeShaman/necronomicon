import {Contact} from "../model/contact.model";
import React, {createContext, FC, ReactNode, useEffect, useState} from "react";
import {getContacts} from "../service/client.service";

export interface ContactContextValue {
    contacts: Contact[],
    findContacts: (query: string) => void,
    addContact: (contact: Contact) => void,
    updateContact: (contact: Contact) => void,
    removeContact: (contactId: number) => void
}

type State = {
    status: "initial" | "loading" | "success" | "error"
    contactList: Contact[]
    error: string | null
}

export const ContactContext = createContext<ContactContextValue | null>(null)

export const ContactProvider: FC<{ children: ReactNode }> = ({children}) => {
    const [state, setState] = useState<State>({
        status: "initial",
        contactList: [],
        error: null
    })

    function processSearch(query: string) {
        setState({...state, status: "loading"})
        getContacts(query)
            .then((data) => {
                setState({status: "success", error: null, contactList: data});
            })
            .catch((error) => {
                setState({status: "error", error, contactList: []})
            })
    }

    useEffect(() => {
        processSearch("")
    }, [])

    function addContact(contact: Contact){
        setState({...state, contactList: state.contactList.concat([contact])})
    }

    function updateContact(contact: Contact) {
        setState({...state, contactList: state.contactList.map((el) =>
            el.id === contact.id ? {...contact} : el)})
    }

    function removeContact(contactId: number) {
        setState({...state, contactList: state.contactList.filter((el) =>
            el.id !== contactId)})
    }

    return (
        <ContactContext.Provider
            value={{
                contacts: state.contactList,
                findContacts: processSearch,
                addContact,
                updateContact,
                removeContact
            }}
        >
            {children}
        </ContactContext.Provider>
    )
}