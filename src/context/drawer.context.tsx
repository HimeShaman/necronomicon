import React, {createContext, FC, ReactNode, useContext, useEffect, useState} from "react";
import {City} from "../model/city.model";
import {createContact, getAvatars, getCities, getDivinities, updateContact} from "../service/client.service";
import {Divinity} from "../model/divinity.model";
import {Avatar} from "../model/avatar.model";
import {Contact, FormContact} from "../model/contact.model";
import {ContactContext} from "./contact.context";

export type NewContactAction = {
    type: "create"
}

export type EditContactAction = {
    type: "edit",
    contactId: number
}

export interface DrawerContextValue {
    opened: boolean
    setOpened: (opened: boolean, action: NewContactAction | EditContactAction | null) => void
    action: NewContactAction | EditContactAction | null
    cities: City[]
    divinities: Divinity[]
    avatars: Avatar[]
    saveContact: (formContact: FormContact) => void
}

type State = {
    opened: boolean,
    action: NewContactAction | EditContactAction | null
}

export const DrawerContext = createContext<DrawerContextValue | null>(null)

export const DrawerProvider: FC<{ children: ReactNode }> = ({children}) => {
    const contactContext = useContext(ContactContext)

    function saveContact(formContact: FormContact) {
        let id: number

        if(state.action?.type === "edit") {
            id = state.action.contactId
        }
        else {
            const maxId = contactContext?.contacts.map((contact) => contact.id)
                .reduce((previous, next) =>
                    previous > next ? previous : next
                )

            id = maxId ? maxId + 1 : 666
        }

        const contact: Contact = {
            id: id,
            firstname: formContact.firstname ?? "N/A",
            lastname: formContact.lastname ?? "N/A",
            email: formContact.email ?? "N/A",
            birthdate: formContact.birthdate ?? new Date("2006-06-06T00:00:00.000Z"),
            birthplace: formContact.birthplace ?? {
                "id": 6,
                "name": "Dans l'abîme du temps"
            },
            affliction: formContact.affliction ?? "En sommeil",
            afflictionLevel: formContact.afflictionLevel ?? 0,
            divinities: formContact.divinities ?? [],
            avatar: formContact.avatar ?? {
                "id": 5,
                "url": "https://i.ibb.co/vVxn9MP/Daco-4207824.png",
                "alt": "HPLovecraft"
            }
        }

        if(state.action?.type === "edit") {
            updateContact(contact)
                .then(() => {
                    contactContext?.updateContact(contact)
                    setState({...state, opened: false, action: null})
                })
        } else {
            createContact(contact)
                .then(() => {
                    contactContext?.addContact(contact)
                    setState({...state, opened: false, action: null})
                })
        }
    }

    const [state, setState] = useState<State>({opened: false, action: null})
    const [cities, setCities] = useState<City[]>([])
    const [divinities, setDivinities] = useState<Divinity[]>([])
    const [avatars, setAvatars] = useState<Avatar[]>([])


    useEffect(() => {
        getCities()
            .then((data) => {
                setCities(data)
            })
            .catch(error => console.log(error))
    }, [])

    useEffect(() => {
        getDivinities()
            .then((data) => {
                setDivinities(data)
            })
            .catch(error => console.log(error))

    }, [])

    useEffect(() => {
        getAvatars()
            .then((data) => {
                setAvatars(data)
            })
            .catch(error => console.log(error))

    }, [])


    return (
        <DrawerContext.Provider
            value={{
                opened: state.opened,
                setOpened: (opened, action) => {
                    setState({...state, opened, action})
                },
                action: state.action,
                cities: cities,
                divinities: divinities,
                avatars: avatars,
                saveContact
            }}
        >
            {children}
        </DrawerContext.Provider>
    )
}
