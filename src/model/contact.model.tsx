import {Divinity} from "./divinity.model";
import {City} from "./city.model";
import {Avatar} from "./avatar.model";

export type Contact = {
    id: number,
    firstname: string,
    lastname: string,
    email: string,
    birthdate: Date,
    birthplace: City,
    divinities: Divinity[],
    affliction: string,
    afflictionLevel: number,
    avatar: Avatar
}

export type FormContact = {
    firstname?: string,
    lastname?: string,
    email?: string,
    birthdate?: Date,
    birthplace?: City,
    divinities?: Divinity[],
    affliction?: string,
    afflictionLevel?: number,
    avatar?: Avatar
}