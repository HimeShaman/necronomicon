export type Avatar = {
    id: number,
    url: string,
    alt: string
}