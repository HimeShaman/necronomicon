export type Divinity = {
    id: number | null,
    name: string
}